# Oxford-IIIT Pet Convolutional Neural Network

Vivek Datta  
Summer 2019

_________

**Read me before proceeding!**

This CNN was developed and trained on a company desktop (the one with the Antec case). As such, the directory path up until the tensorflow 
folder (in this case, `/home/ramesh/Desktop/Oxford_IIIT_CNN`) must be changed depending on which machine you wish to run this CNN in. 

Also, this CNN provides a structure for training different datasets as well in the future. In order to reuse much of the same pipelines and set up, you will need to find different annotated datasets, generate their TFRecords, and modify config files plus label maps.  
  
Here, the Oxford-IIIT dataset is being used for our training and validation data. It is a groundbreaking dataset that was published in 2012 by O. M. Parkhi and others in Oxford's Visual Geometry Group (VGG), and you can find further information regarding the dataset [here](http://www.robots.ox.ac.uk/~vgg/data/pets/).


You can find a more detailed writeup of the following instructions by clicking [here](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/running_pets.md).

If you do not wish to use Google Cloud Storage, click [this](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/running_locally.md). Most of the steps you'll want to follow are in the first link, but if you would like to perform this job locally, I've written this document to help run the CNN natively on your machine. Theoretically, it would be a much faster training job using GCS, but the commands here are not optimized for this.

Finally, all credits go Google's Tensorflow team for providing pretty much everything, ranging from the models and APIs along with the sample configuration files and most of the instructions below. I have made edits wherever necessary and appropriate in order to write more focused and targeted instructions for anyone in the company using this process. 

Therefore, this guide doesn't seek to rewrite the above two links in this section, but to instead illustrate and guide others through my process behind setting up this neural network. For any further questions, refer to the above URLs or contact a current employee in the company. Finally, if all of the above fails, please do reach out to me if anything related to this process is in question and I promise I will be more than happy to try and help out. 


_________

## Installing Tensorflow and the Tensorflow Object Detection API

_________
### Tensorflow

For detailed steps to install Tensorflow, follow the [Tensorflow installation instructions](https://www.tensorflow.org/install/). A typical user can install Tensorflow using one of the following commands:


**CPU-Only**
``` bash 
pip install tensorflow
```

Yep, it's that easy. You can move on to the Object Detection API installation now and make some more progress through this shockingly long guide. Bad news, there's a lot left to do and we've only scratched the surface. Good news, I promise you, you're gonna be fine. 

**GPU-Only**

*Author's Note: Create a new Anaconda environment to install everything (the libraries for setting up the GPU as well as the rest of the instructions within this section). You will be running training and testing from this environment itself! I've included the environment file I used, which I named `tensorflow-gpu.yml`, so you can get an idea of how I did things. Or to make things easier, you can also import this environment on Anaconda and use it on your own machine as well.*

For installing on Ubuntu 18.04 (CUDA 10), do the following:
``` bash 
pip install tensorflow-gpu  # stable

# Add NVIDIA package repositories
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-repo-ubuntu1804_10.0.130-1_amd64.deb
sudo dpkg -i cuda-repo-ubuntu1804_10.0.130-1_amd64.deb
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
sudo apt-get update
wget http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64/nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
sudo apt install ./nvidia-machine-learning-repo-ubuntu1804_1.0.0-1_amd64.deb
sudo apt-get update

# Install NVIDIA driver
sudo apt-get install --no-install-recommends nvidia-driver-418
# Reboot. Check that GPUs are visible using the command: nvidia-smi

# Install development and runtime libraries (~4GB)
sudo apt-get install --no-install-recommends \
    cuda-10-0 \
    libcudnn7=7.6.0.64-1+cuda10.0  \
    libcudnn7-dev=7.6.0.64-1+cuda10.0
```

Now, open up the Python interpreter by typing in `python` in the Terminal. Run the following commands:
``` bash
from tensorflow.python.client import device_lib

device_lib.list_local_devices()
```

If everything goes right, you will see an output similar to this which will have both the CPU and GPU listed as available devices. The GPU information will be more specific to the actual model you are using:

``` bash 
[{
    name: "/device:CPU:0",
    device_type: "CPU",
    memory_limit: 268435456,
    locality {},
    incarnation: 12584189039274141042
},{
    name: "/device:GPU:0",
    device_type: "GPU",
    memory_limit: 3252486144,
    locality {
      bus_id: 1,
      links {}
    },
    incarnation: 16344452236433767630, 
    physical_device_desc: "device: 0, name: GeForce GTX 1070 Ti, pci bus id: 0000:01:00.0, compute capability: 6.1"
]

```
_________
### Tensorflow Object Detection API

Tensorflow Object Detection API depends on the following libraries:

*   Protobuf 3.0.0
*   Python-tk
*   Pillow 1.0
*   lxml
*   tf Slim (which is included in the "tensorflow/models/research/" checkout)
*   Jupyter notebook
*   Matplotlib
*   Tensorflow (>=1.12.0)
*   Cython
*   contextlib2
*   cocoapi


These libraries can be installed on Ubuntu 18.04 using via apt-get:

``` bash
sudo apt-get install protobuf-compiler python-pil python-lxml python-tk
pip install --user Cython
pip install --user contextlib2
pip install --user jupyter
pip install --user matplotlib
```
Alternatively, users can install dependencies using pip:

``` bash
pip install --user Cython
pip install --user contextlib2
pip install --user pillow
pip install --user lxml
pip install --user jupyter
pip install --user matplotlib
```
Note: sometimes "sudo apt-get install protobuf-compiler" will install Protobuf 3+ versions for you and some users have issues when using 3.5. If that is your case, try the manual installation.

### COCO API installation
Download the cocoapi and copy the pycocotools subfolder to the tensorflow/models/research directory if you are interested in using COCO evaluation metrics. The default metrics are based on those used in Pascal VOC evaluation. 
To use the COCO object detection metrics add metrics_set: "coco_detection_metrics" to the eval_config message in the config file:

``` bash
git clone https://github.com/cocodataset/cocoapi.git
cd cocoapi/PythonAPI
make
cp -r pycocotools <path_to_tensorflow>/models/research/
```

### Protobuf Compilation
The Tensorflow Object Detection API uses Protobufs to configure model and training parameters. Before the framework can be used, the Protobuf libraries must be compiled. This should be done by running the following command from the tensorflow/models/research/ directory:

``` bash
# From tensorflow/models/research/*
protoc object_detection/protos/*.proto --python_out=.
```
Note: If you're getting errors while compiling, you might be using an incompatible protobuf compiler. If that's the case, use the following manual installation:

#### Manual protobuf-compiler installation and usage
**If you are on linux: **

Download and install the 3.0 release of protoc, then unzip the file:


``` bash
# From tensorflow/models/research/
wget -O protobuf.zip https://github.com/google/protobuf/releases/download/v3.0.0/protoc-3.0.0-linux-x86_64.zip
unzip protobuf.zip
Run the compilation process again, but use the downloaded version of protoc
``` 

``` bash
# From tensorflow/models/research/
./bin/protoc object_detection/protos/*.proto --python_out=.
``` 

**If you are on MacOS:**

If you have homebrew, download and install the protobuf with ```brew install protobuf```

Alternatively, users can install dependencies using pip:  
  
``` bash
curl -OL https://github.com/google/protobuf/releases/download/v3.3.0/$PROTOC_ZIP
sudo unzip -o $PROTOC_ZIP -d /usr/local bin/protoc
rm -f $PROTOC_ZIP
```
Run the compilation process again:
 
``` bash
# From tensorflow/models/research/
protoc object_detection/protos/*.proto --python_out=.
```

### Adding Libraries to PYTHONPATH
When running locally, the tensorflow/models/research/ and slim directories should be appended to PYTHONPATH. This can be done by running the following from tensorflow/models/research/:  


``` bash
# From tensorflow/models/research/
export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
``` 

Note: This command needs to run from every new terminal you start. If you wish to avoid running this manually,  you can add it as a new line to the end of your ~/.bashrc file, replacing `pwd` with the absolute path of `tensorflow/models/research` on your system.

### Testing the Installation
You can test that you have correctly installed the Tensorflow Object Detection API by running the following command:

``` bash
python object_detection/builders/model_builder_test.py
``` 
_________

## Getting the Oxford-IIIT Pets Dataset

*Author's Note: If you would like to use a different dataset instead of pets for object detection, you will need to find a dataset that has images and annotations for the groundtruth. These steps would still largely be followed, but you would most likely need to find a script that would convert the images to TFRecords for you or to likely just download them raw from online somewhere. Also, you will need to obtain a different label map or write one yourself. For reference in any of these steps, you can still use this guide to visualize the process you'll need to take.*


The images for the Oxford-IIIT pets dataset will be in images.tar.gz and the groundtruth data lies in annotations.tar.gz. The Oxford-IIIT Pet data set is located [here](http://www.robots.ox.ac.uk/~vgg/data/pets/). To obtain the above, type:  

``` bash
# From tensorflow/models/research/
wget http://www.robots.ox.ac.uk/~vgg/data/pets/data/images.tar.gz
wget http://www.robots.ox.ac.uk/~vgg/data/pets/data/annotations.tar.gz
tar -xvf images.tar.gz
tar -xvf annotations.tar.gz
```
  
Now, both must be converted to TFRecords, which is the format used by the model for training and evaluating on the dataset. To do this, run:

``` bash 
python object_detection/dataset_tools/create_pet_tf_record.py \
    --label_map_path=object_detection/data/pet_label_map.pbtxt \
    --data_dir=`pwd` \
    --output_dir=`pwd`

```

You should end up with two 10-sharded TFRecord files named `pet_faces_train.record-?????-of-00010` and `pet_faces_val.record-?????-of-00010` in the `tensorflow/models/research/` directory.

The label map for the Pet dataset can be found at `object_detection/data/pet_label_map.pbtxt`.

_________


## Downloading a COCO-pretrained Model for Transfer Learning

Training a very accurate model will take days, if not weeks. And honestly, [ain't nobody got the time for that](https://www.youtube.com/watch?v=ydmPh4MXT3g). In order to speed up training, we'll take an object detector trained on a different dataset (COCO), and reuse some of its
parameters to initialize our new model.

Download the [COCO-pretrained Faster R-CNN with Resnet-101 model](http://storage.googleapis.com/download.tensorflow.org/models/object_detection/faster_rcnn_resnet101_coco_11_06_2017.tar.gz). Unzip the contents of the folder and copy the `model.ckpt*` files into `tensorflow/models/research`. 

``` bash
wget http://storage.googleapis.com/download.tensorflow.org/models/object_detection/faster_rcnn_resnet101_coco_11_06_2017.tar.gz
tar -xvf faster_rcnn_resnet101_coco_11_06_2017.tar.gz
```

Remember the path where you uploaded the model checkpoint to, as we will need it in the following step.

_________

## Configuring the Object Detection Pipeline

In the Tensorflow Object Detection API, the model parameters, training parameters and eval parameters are all defined by a config file. If you would like to have control over this if you are training on a different dataset, look over [here](configuring_jobs.md) as a good place to start.

For the purpose of this example, we will use some predefined templates provided with the source code. In the `object_detection/samples/configs` folder, there are skeleton object_detection configuration files. We will use `faster_rcnn_resnet101_pets.config` as a starting point for configuring the pipeline. Open the file with a text editor and make the necessary changes.

We'll need to configure some paths in order for the template to work. Search the file for instances of `PATH_TO_BE_CONFIGURED` and replace them with the directory you wish to use. For all of the files I have committed on this repository, I have already gone ahead and changed
them to work for the directory on the computer I had been using for when I had trained the CNN. You will need to change this if you are not using this machine or if the path on the desktop has somehow been changed in any way. 

_________

## Training and Evaluating

At this point, make sure and check that your directory structure, within the tensorflow folder, is something similar to the following:

```
+data
  -label_map file
  -train TFRecord file
  -eval TFRecord file
+models
  + model
    -pipeline config file
    +train
    +eval
```

The following section will be split up into two parts: one command has some starter code you can use to fill in the paths you have stored everything in, and the other command is specific to the machine I had set up to train my network. 

```bash
# Starter instructions

PIPELINE_CONFIG_PATH={path to pipeline config file}
MODEL_DIR={path to model directory}
NUM_TRAIN_STEPS=50000
SAMPLE_1_OF_N_EVAL_EXAMPLES=1
python object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --num_train_steps=${NUM_TRAIN_STEPS} \
    --sample_1_of_n_eval_examples=$SAMPLE_1_OF_N_EVAL_EXAMPLES \
    --alsologtostderr

```

where ${PIPELINE_CONFIG_PATH} points to the pipeline config and ${MODEL_DIR} points to the directory in which training checkpoints and events will be written to. `NUM_TRAIN_STEPS` simply specifies how many training steps you wish to take (as the name of the variable should, hopefully, imply) and `SAMPLE_1_OF_N_EVAL_EXAMPLES` will randomly pick a set of validation examples to test to evaluate the classification accuracy. These examples can be seen on TensorBoard along with their specific metrics such as mAP and Bounding Box Loss. 

**Note that this will automatically switch back and forth between training and evaluation. If, when you run the above command on the Terminal, the output is hung up on something similar to the following:** 

``` bash
INFO:tensorflow:Calling model_fn.
INFO:tensorflow:Done calling model_fn.
INFO:tensorflow:Starting evaluation at ...
INFO:tensorflow:Graph was finalized.
INFO:tensorflow:Restoring parameters from ./test/model.ckpt-100
INFO:tensorflow:Running local_init_op.
INFO:tensorflow:Done running local_init_op.
```

This means that **the dataset input is being repeated and testing the training performance on the evaluation dataset is about to happen. It is not a bug or a problematic sign, it simply means that training and evaluation is happening.** If you wish to end the training/evaluation process at any point in time before you reach `NUM_TRAIN_STEPS` amount of work, you can close the Terminal or simply click `Control + C`. When you do re-run the above commands, it will pick off right where it left off and train from its last checkpoints. 

Here's what I did to run the training and evaluation process for the machine I had worked on, for reference:

``` bash 
# Personal process I used to run the training (Click Control + C whenever you would like to stop training and evaluation) 

PIPELINE_CONFIG_PATH=/home/ramesh/Desktop/Oxford_IIIT_CNN/tensorflow/models/research/faster_rcnn_resnet101_coco_11_06_2017/faster_rcnn_resnet101_pets.config
MODEL_DIR=/home/ramesh/Desktop/Oxford_IIIT_CNN/tensorflow/models/research/faster_rcnn_resnet101_coco_11_06_2017
NUM_TRAIN_STEPS=50000
SAMPLE_1_OF_N_EVAL_EXAMPLES=10
python object_detection/model_main.py \
    --pipeline_config_path=${PIPELINE_CONFIG_PATH} \
    --model_dir=${MODEL_DIR} \
    --num_train_steps=${NUM_TRAIN_STEPS} \
    --sample_1_of_n_eval_examples=$SAMPLE_1_OF_N_EVAL_EXAMPLES \
    --alsologtostderr

```
_________

## Running TensorBoard

TensorBoard is a free tool provided by Google to visualize important training metrics of the neural network as time goes on and to compare model predictions with the groundtruth data from the evaluation set. If you are using the recommended dictionary structure, you can access TensorBoard by doing the following:

Open a new Terminal window while the above training job is running. Then, type:

```bash
MODEL_DIR={path to model directory}

tensorboard --logdir=${MODEL_DIR}
```
where `${MODEL_DIR}` points to the directory that contains the train and eval directories. Note it may take Tensorboard a couple minutes to populate with data.

_________

## Exporting The Final Model 
After your model has been trained, you should export it to a Tensorflow graph proto. In your `MODEL_DIR` directory, refer to the largest `CHECKPOINT_NUMBER` as the one you wish to export. The checkpoint will typically consist of three files:

* `model.ckpt-${CHECKPOINT_NUMBER}.data-00000-of-00001`
* `model.ckpt-${CHECKPOINT_NUMBER}.index`
* `model.ckpt-${CHECKPOINT_NUMBER}.meta`

After you've identified a candidate checkpoint to export, run the following command from `tensorflow/models/research/`:

```bash
# From tensorflow/models/research/
python object_detection/export_inference_graph.py \
    --input_type image_tensor \
    --pipeline_config_path object_detection/samples/configs/faster_rcnn_resnet101_pets.config \
    --trained_checkpoint_prefix model.ckpt-${CHECKPOINT_NUMBER} \
    --output_directory exported_graphs
```

Afterwards, you should see a directory named `exported_graphs` containing the SavedModel and frozen graph. Congratulations, you have finished training, evaluating, and exporting your trained model (which I hope is actually kinda cool to you)! 😀

_________

## Final Steps

If you'd like to test on a few example images right out of the box, try out the Jupyter notebook demo. To run the Jupyter notebook, run the following command from `tensorflow/models/research/object_detection`:

```
# From tensorflow/models/research/object_detection
jupyter notebook
```
The notebook should open in your favorite web browser. Click the object_detection_tutorial.ipynb link to open the demo. Note that you will need to make a few adjustments to the Jupyter Notebook in order for it to come out correctly: 

* We have already downloaded the model, so there is no need to do this again. Comment out or delete `MODEL_NAME`, `MODEL_FILE`, and `DOWNLOAD_BASE`.  

* Delete whatever currently set to `PATH_TO_FROZEN_GRAPH`. You will need to manually copy down the full path where the exported graph is at. Go to your `'/frozen_inference_graph.pb'` file in `exported_graphs` and go to its file properties. Then, copy down the parent directory containing the full path to get there. The full path at the end should end up looking something like `/home/.../exported_graphs/frozen_inference_graph.pb`. Assign this string to `PATH_TO_FROZEN_GRAPH`.  

* In addition, you will need to do the above for `PATH_TO_LABELS`. Get the full path and set the variable to wherever `pet_label_map.pbtxt` is located.  

* Comment out or delete the Jupyter Notebook cell under the header titled "**Download Model**". 

* You will need to set up and create a `test_images` directory. The way I did it was by selecting a handful of images from the Oxford-IIIT dataset (located in the images folder at whatever directory you downloaded them) and then copying and pasting them into a directory I named `test_images`, which I placed under the `tensorflow` folder and on the same level as `models` and `data`. I then set `PATH_TO_TEST_IMAGES_DIR` to be the full directory path where I had created and set up `test_images`.  

* Rename each image in the `test_images` folder to be `image1.jpg`, `image2.jpg`, `image3.jpg`, etc. because within the code itself, `TEST_IMAGE_PATHS` was set to be a list comprehension that looped through all of the images with this structured file name.  

* Within `TEST_IMAGE_PATHS`, I had to modify the range over which it looped over too. It is set to `for i in range(1,3)`, but if you are loading, for example, N images in your `test_images` directory, you will need to modify this line to be `for i in range(1, N+1)` as the `range` keyword in Python creates an iterator that starts at the first number and ends just before the second number. 

Note that you can be flexible with how you approach the last three bullet points (how you get to your test image directory and how you name your test image files), but I decided simply to modify the existing code and not change too much of it. It is definitely possible to not need to rename all of the images per sé if you are able to change the `TEST_IMAGE_PATHS` variable correctly on your own, but I have not done this (for the sake of treating the Jupyter Notebook steps as more of a proof of concept rather than a finished model to be deployed ASAP). 


After running all the cells in the Jupyter Notebook, you will see all of the images in `test_images` resized and classified, with a bounding box surrounding it and a percentage dictating how accurate the model believes this prediction to be.


Some (optional) steps you can also do at this point are to use much of these instructions, but instead redownload and retrain your model on a different set of data (as long as you are able to create valid TFRecords and get a correct label map file set up) and change the pipeline configuration or file to modify any of the hyperparameters to your own liking. In addition, you can try running this on a GPU or online on Google Cloud Storage to highly speed up training and evaluation times.  
  
At this point though, this will be left to you, as you have now walked through the entire development process. Our goal of training a neural network to become a pets detector has been achieved.

_________
